package saint_valentin;

import etwin.Obfu;

import hf.entity.Shoot;

import hf.Entity;
import hf.mode.GameMode;
import hf.Data;

@:hfTemplate
class FlecheAmour extends Shoot {

	public function new(): Void {
		super(null);
		this.shootSpeed = 4.5;
	}

	public static function attach(g: GameMode, x: Float, y: Float, dark : Bool): FlecheAmour {
		var fleche: FlecheAmour = cast g.depthMan.attach(Obfu.raw("flecheAmour"), g.root.Data.DP_SHOTS);
			fleche.initShoot(g, x, y);
			if (dark){
				fleche.shootSpeed = 5;
			}
		return fleche;
	}

	override public function hit(e: Entity): Void {
		if ((e.types & this.game.root.Data.PLAYER) > 0) {
			var joueur: hf.entity.Player = cast e;
			var explosionCoeur = this.game.fxMan.attachFx(this.x-20, this.y-15, Obfu.raw("$explodeCoeur"));
				explosionCoeur.mc._xscale = 20;
				explosionCoeur.mc._yscale = 20;
			joueur.killHit(0);
			this.destroy();
		}
	}
}
