package saint_valentin.effet_item;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class LettreDAmour implements ISpec {

	public var id(default, null): Int = 121;
	private var STATIC_MESSAGE : Int = 600;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		var score : Int = Math.round(specMan.player.score/1000)*100;
		if (score > 0){
			specMan.player.getScore(specMan.player, score);
		}
		specMan.player.game.attachPop("\n" + hf.Lang.get(STATIC_MESSAGE), false);
		for (joueur in specMan.game.getPlayerList()) {
			joueur.setBaseAnims(hf.Data.ANIM_PLAYER_WALK_L, hf.Data.ANIM_PLAYER_STOP_L);
		}
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}

}
