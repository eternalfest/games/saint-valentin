package  saint_valentin.effet_item;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class VieLubrique implements ISpec {

	public var id(default, null): Int = 119;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		specMan.player.lives++;
		specMan.game.gi.setLives(specMan.player.pid, specMan.player.lives);
		specMan.game.fxMan.attachShine(item.x, item.y - hf.Data.CASE_HEIGHT * 0.5);
		specMan.game.randMan.remove(hf.Data.RAND_ITEMS_ID, item.id);
	}

	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}
}
