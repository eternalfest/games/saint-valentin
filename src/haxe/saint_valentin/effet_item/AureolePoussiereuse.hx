package  saint_valentin.effet_item;

import hf.mode.GameMode;
import hf.entity.Item;
import vault.ISpec;

class AureolePoussiereuse implements ISpec {

	public var id(default, null): Int = 120;

	public function new() {}

	public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item : Item): Void {
		specMan.player.head = 13;
        specMan.player.replayAnim();
	}
	public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void{}
}
