package saint_valentin;

import etwin.Obfu;
import patchman.IPatch;
import patchman.Ref;

import merlin.IAction;
import merlin.IActionContext;

import hf.entity.Mover;
import hf.entity.item.SpecialItem;
import hf.mode.GameMode;
import hf.Data;

import etwin.flash.filters.GlowFilter;

import saint_valentin.FlecheAmour;

@:hfTemplate
class CupidonBoss extends hf.entity.Mover {
	public static var CUPIDON_VIES = 10;

	public static var CUPIDON_VIES_MIN_FLECHES 			= 7;
	public static var CUPIDON_VIES_MIN_FLECHES_TOUCHE 	= 5;
	public static var CUPIDON_VIES_MIN_DASH 			= 3;

	public static var CUPIDON_COEFF_VITESSE		= 0.1;

	public static var CUPIDON_CHANGE_PLAYER_PROBA = 4;

	public static var CUPIDON_TEMPS_FLECHE = 1;
	public static var CUPIDON_TEMPS_DASH = 4;
	public static var CUPIDON_TEMPS_HALT = 12;
	public static var CUPIDON_TEMPS_HALT_PHASE = 5;
	public static var CUPIDON_TEMPS_MORT = 5;

	public static var CUPIDON_PROBA_FLECHE = 20;
	public static var CUPIDON_PROBA_DASH = 1000;

	public var listeVies 	= new Array();
	public var vies			= CUPIDON_VIES;

	public var tempsFleche : Float;
	public var tempsHalt : Float;
	public var tempsDash : Float;
	public var tempsMort : Float;

	public var dark : Bool;
	public var mort = false;
	public var immobile = true;
	public var speedFactor : Float;

	public var player : hf.entity.Player;

	public static function attach(g: GameMode, dark: Bool): CupidonBoss {
	  var v3 = Obfu.raw("cupidonBoss");
    var v4: CupidonBoss = cast g.depthMan.attach(v3, g.root.Data.DP_BADS);
		v4.initBoss(g, dark);
		v4.gotoAndStop(dark ? 2 : 1);
    return v4;
  }

  @:keep
  public function freeze(t: Float) {
	  patchman.DebugConsole.log("!freeze");
		if(this.immobile){
			this.game.destroyList(this.game.root.Data.BOMB);
			this.reduitVie();
		}
	}

  public function initBoss(g: GameMode, dark: Bool): Void {
	  super.init(g);
	  this.game.fl_clear = false;
    this.moveTo(g.root.Data.GAME_WIDTH * 0.5, g.root.Data.GAME_HEIGHT * 0.8);
	  this.playAnim(cast {id : (dark ? 2 : 1), loop : true});
    this._xscale = -this.scaleFactor * 100;

    this.tempsDash = CUPIDON_TEMPS_DASH* this.game.root.Data.SECOND * 3;
    this.tempsFleche = CUPIDON_TEMPS_FLECHE* this.game.root.Data.SECOND * 3;
    this.tempsHalt = CUPIDON_TEMPS_HALT* this.game.root.Data.SECOND;

    for (i in 0 ... CupidonBoss.CUPIDON_VIES){
		  var vie = this.game.world.view.attachSprite(Obfu.raw("vieCupidon"),10,250 - 10*CupidonBoss.CUPIDON_VIES + 20*i, false);
		  this.listeVies.push(vie);
	  }

	  this.register(g.root.Data.BAD_CLEAR);
	  this.register(g.root.Data.BAD);
    this.register(g.root.Data.BOSS);
	  this.immobile = false;
	  this.dark = dark;
	  this.player = g.getPlayerList()[0];
	  this.initSpeed();
	  this.endUpdate();
  }

	public function dash() {
		if (vies > CUPIDON_VIES_MIN_DASH && !dark)
			return;
		var spriteDash = this.game.fxMan.attachFx(this.x-20, this.y-30, Obfu.raw("dash"));
		if (this.dir > 0){
			spriteDash.mc._x += 100;
			this.x += 100;
		}else{
			spriteDash.mc._x -= 100;
			this.x -= 100;
		}
		this.tempsDash = CUPIDON_TEMPS_DASH * this.game.root.Data.SECOND;
	}

	public function halt(){
		this.immobile = true;
		this.dx = 0;
		this.dy = 0;
		this.aura();
		this.tempsHalt = (CUPIDON_TEMPS_HALT_PHASE - (dark ? 1 : 0))* this.game.root.Data.SECOND;
		var glow = new etwin.flash.filters.GlowFilter();
			glow.quality = 1;
			glow.color = 9175040;
			glow.strength = 50;
			glow.blurX = 8;
			glow.blurY = 8;
			glow.alpha = 1.0;
		filters = [glow];

	}

	public function initSpeed(){
		var coeff = 1.0;
		if (this.player.speedFactor > 1){
			coeff += 1.5;
		}
		if (this.dark){
			coeff += .5;
		}
		this.speedFactor = coeff;
	}

	public function reduitVie(){
		var vie = cast listeVies[listeVies.length - 1];
			vie.removeMovieClip();
			listeVies.pop();
			this.initSpeed();
			this.tempsHalt = -1;
			this.vies -= 1;
		if(dark){
			var joueurs = this.game.getPlayerList();
			this.game.shake(this.game.root.Data.SECOND * 0.5, 5);
			for (i in 0...joueurs.length) {
				var joueur = joueurs[i];
				if (joueur.fl_stable) {
					if (joueur.fl_shield) {
						joueur.dy = -8;
					} else {
						joueur.knock(this.game.root.Data.PLAYER_KNOCK_DURATION);
					}
				}
			}
		}
		if(vies == 0){
			this.mort = true;
			this.tempsMort = this.game.root.Data.SECOND * CUPIDON_TEMPS_MORT;
		}
	}

	override public function hit(e: hf.Entity): Void {
		if(this.mort)
			return;
		if ((e.types & this.game.root.Data.PLAYER) > 0) {
			var 	player: hf.entity.Player = cast e;
			if(!player.fl_shield){
				this.game.fxMan.attachFx(this.x, this.y, Obfu.raw("$explodeCoeur"));
				player.killHit(null);
			}
		}
		if (!immobile && (e.types & this.game.root.Data.BOMB) > 0){
			var 	bombe: hf.entity.Player = cast e;
					var explosion = this.game.fxMan.attachFx(bombe.x - 15, bombe.y-25, Obfu.raw("$explodeCoeur"));
						explosion.mc._xscale = 50;
						explosion.mc._yscale = 50;
					bombe.destroy();
					if (vies <= CUPIDON_VIES_MIN_FLECHES_TOUCHE || dark)
						this.arc(0);
		}
	};

	public function aura(){
		var explosion = this.game.fxMan.attachFx(this.x - 60, this.y - 80, Obfu.raw("$aura"));
			explosion.mc._alpha = 75;
	};

	public function arc(? type : Int){
		if(type == null){
			if (vies > CUPIDON_VIES_MIN_FLECHES && !dark)
				return;
			var fleche = FlecheAmour.attach(this.game, x,y, dark);
			var explosionCoeur = this.game.fxMan.attachFx(this.x - 30, this.y-15, Obfu.raw("$explodeCoeur"));
				explosionCoeur.mc._xscale = 20;
				explosionCoeur.mc._yscale = 20;
			if (this.dir < 0) {
				fleche.moveLeft(fleche.shootSpeed);
			} else {
				fleche.moveRight(fleche.shootSpeed);
			}
		}
		else{
			var nFleches = (CUPIDON_VIES - vies) + 1 + (dark ? 4 : 0);
			var ajouter = this.game.root.Std.random(360)+0.01;
			for (i in 0...nFleches){
				var fleche = FlecheAmour.attach(this.game, x,y, dark);
				var angle = i*360/nFleches + ajouter;
				fleche.moveToAng(angle, fleche.shootSpeed);
				fleche.rotation = angle;
			}
		}
		this.tempsFleche = CUPIDON_TEMPS_FLECHE * this.game.root.Data.SECOND;
	};

	public override function update(){
		super.update();
		if (!mort){
			this.game.huTimer = 0;
			if (this.game.root.Std.random(1000) <= CupidonBoss.CUPIDON_CHANGE_PLAYER_PROBA)
				this.player = cast this.game.getOne(this.game.root.Data.PLAYER);
			this.tempsDash -= this.game.root.Timer.tmod;
			this.tempsFleche -= this.game.root.Timer.tmod;
			this.tempsHalt -= this.game.root.Timer.tmod;
			if (!this.immobile){
				if(this.tempsHalt < 0){
					this.halt();
				}else{
					if(this.tempsDash < 0 && this.game.root.Std.random(1000) <= CupidonBoss.CUPIDON_PROBA_DASH){
						this.dash();
					}
					if (this.tempsFleche < 0 && (this.game.root.Std.random(1000) <= CupidonBoss.CUPIDON_PROBA_FLECHE || dark)){
						this.arc();
					}
				}
				this.moveToTarget(this.player, 2 + (CUPIDON_VIES - this.vies) * CUPIDON_COEFF_VITESSE * this.speedFactor);
			}else{
				if (this.game.root.Std.random(1000) <= 40){
					var explosion = this.game.fxMan.attachFx(this.x - 60, this.y - 80, Obfu.raw("$aura"));
						explosion.mc._alpha = 75;
				}
				this.dx = 0;
				this.dy = 0;
				if(this.tempsHalt < 0){
					this.player = cast this.game.getOne(this.game.root.Data.PLAYER);
					this.filters = null;
					this.immobile = false;
					this.arc(0);
					this.game.fxMan.attachFx(this.x, this.y, Obfu.raw("$explodeCoeur"));
					this.tempsHalt = CUPIDON_TEMPS_HALT* this.game.root.Data.SECOND;
					this.tempsDash = CUPIDON_TEMPS_DASH * this.game.root.Data.SECOND;
					this.tempsFleche = CUPIDON_TEMPS_FLECHE * this.game.root.Data.SECOND;
				}
			}
		}else{
			this.tempsMort -= this.game.root.Timer.tmod;
			this.dx = 0;
			this.dy = 0;
			if (this.tempsMort > 0){
				if (this.game.root.Std.random(4) == 0){
					var explosion = this.game.fxMan.attachFx(this.x - 30 + this.game.root.Std.random(60), this.y- 20 + this.game.root.Std.random(40), Obfu.raw("$explodeCoeur"));
						explosion.mc._xscale = this.game.root.Std.random(50) + 50;
						explosion.mc._yscale = explosion.mc._xscale;
				}
				if (this.game.root.Std.random(4) == 0){
					var v2 = this.game.depthMan.attach("hammer_fx_strike", this.game.root.Data.FX);
						v2._y = this.y - this.game.root.Std.random(60) + 30;
						v2._x = this.game.root.Data.GAME_WIDTH * 0.5;
						var v3 = this.game.root.Std.random(2) * 2 - 1;
						v2._xscale = 100 * v3;
						this.alpha -= 1;
						this.minAlpha -= 1;
				}

			}else{
				this.game.root.entity.item.SpecialItem.attach(this.game, this.x, this.y, 120, null);
				this.game.shake(this.game.root.Data.SECOND, 5);
				this.game.fl_clear = true;
				this.game.fxMan.attachExit();
				this.destroy();
			}
		}
	};

	public function new(){
		super(null);
		this.fl_hitGround = false;
		this.fl_hitCeil = false;
		this.fl_hitWall = false;
		this.fl_hitBorder = true;
		this.fl_gravity = false;
		this.fl_friction = false;
		this.fl_moveable = false;
		this.fl_alphaBlink = false;
	};

	override public function endUpdate(): Void {
		super.endUpdate();
		this.dir = this.dx > 0 ? 1 : -1;
		this._xscale = this.dir*100;
	}
}

class CupidonAction implements IAction {

  public var name(default, null): String = Obfu.raw("attachCupidon");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var boss = CupidonBoss.attach(game, false);
		return true;
	}
}
class DarkupidonAction implements IAction {

  public var name(default, null): String = Obfu.raw("attachDarkupidon");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var boss = CupidonBoss.attach(game, true);
		return true;
	}
}
class TempsReset implements IAction {

  public var name(default, null): String = Obfu.raw("tempsReset");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		game.huTimer = 0;
		return true;
	}
}
