package saint_valentin;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

import hf.Entity;
import hf.mode.GameMode;

@:hfTemplate
class BulleLoreSprite extends Entity {

	public static function attach(g: GameMode, x : Float, y : Float, id : Int): BulleLoreSprite {
		var bulle: BulleLoreSprite = cast g.depthMan.attach(Obfu.raw("bulleLore"), g.root.Data.DP_SUPA);
			bulle.initBulle(g, x, y, id);
		return bulle;
	}

	public function initBulle(game : GameMode, x : Float, y : Float, id : Int) : Void{
		this.init(game);
		this.x = x;
		this.y = y;
		var texte = this.createTextField('text', 1, -85,-90,180,78);
			texte.multiline = true;
			texte.wordWrap 	= true;
			texte.html 		= true;
			texte.selectable = false;
			texte.htmlText = "<p align=\"center\">" + game.root.Lang.get(id) +"<p>";
		var scroll = texte.bottomScroll;
		switch (scroll){
			case 1 : texte._y += 29;
			case 2 : texte._y += 21;
			case 3 : texte._y += 13;
			case 4 : texte._y += 5;
		}
	}

	public function new(){
		super(null);
	};
}

class BulleLoreAction implements IAction {

  public var name(default, null): String = Obfu.raw("bulleLore");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

	public function run(ctx: IActionContext): Bool {
		var game = ctx.getGame();
		var x: Null<Float> = ctx.getFloat(Obfu.raw("x"));
		var y: Null<Float> = ctx.getFloat(Obfu.raw("y"));
		var id: Null<Int> = ctx.getInt(Obfu.raw("id"));
		var sid: Null<Int> = ctx.getOptInt(Obfu.raw("sid")).toNullable();
		var bulle = BulleLoreSprite.attach(game,x,y, id);
		game.world.scriptEngine.killById(sid);
		if (sid != null)
			bulle.scriptId = sid;
		game.fxMan.attachFx(x, y, "popShield");
		return true;
	}
}
