package saint_valentin;

import merlin.IAction;
import merlin.IActionContext;

import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.FrozenArray;
import etwin.Obfu;

import etwin.flash.Key;

import custom_clips.CustomClips;

import hf.entity.Player;
import hf.Hf;
import hf.GameManager;
import hf.FxManager;

import saint_valentin.BulleLoreSprite;
import saint_valentin.CupidonBoss;

import saint_valentin.effet_item.VieLubrique;
import saint_valentin.effet_item.VieSentimentale;
import saint_valentin.effet_item.AureolePoussiereuse;
import saint_valentin.effet_item.LettreDAmour;
import quete.Quetes;

import vault.ISpec;

@:build(patchman.Build.di())
class SaintValentin {

	public var custom_clips: CustomClips;
	public var quetes: Quetes;

	@:diExport
	public var PATCH_COEURS_VOLANTS(default, null) : IPatch =
		Ref.auto(Player.update)
		.after(function(hf: Hf, self: Player) {
			if (hf.Std.random(4) == 0) {
				var v3 = self.game.fxMan.attachFx(self.x + hf.Std.random(15) * (hf.Std.random(2) * 2 - 1) - 5, self.y - hf.Std.random(10) - self._height, "$hammer_fx_coeur");
				v3.mc._xscale = hf.Std.random(70) + 30;
				v3.mc._yscale = v3.mc._xscale;
			}
		});

	@:diExport
	public var PATCH_REGISTRE_BULLELORE(default, null) : IPatch =
		Ref.auto(GameManager.registerClasses)
		.after(function(hf: Hf, self: GameManager) : Void {
			 hf.Std.registerClass(Obfu.raw("bulleLore"), BulleLoreSprite.getClass(hf));
			 hf.Std.registerClass(Obfu.raw("cupidonBoss"), CupidonBoss.getClass(hf));
			 hf.Std.registerClass(Obfu.raw("flecheAmour"), FlecheAmour.getClass(hf));
		});

	@:diExport
	public var PATCH_BOSS_CUPIDON_DANGER(default, null) : IPatch =
		Ref.auto(hf.mode.Adventure.isBossLevel)
		.wrap(function(hf: Hf, self, id : Int, old) : Bool {
			return id == 21;
		});

	@:diExport
	public var PATCH_DEGUISEMENT(default, null) : IPatch =
		Ref.auto(hf.mode.GameMode.getControls)
		.wrap(function(hf: Hf, self: hf.mode.GameMode, old) : Void {
			if (self.fl_disguise && Key.isDown(68) && self.keyLock != 68) {
				var joueur = self.getPlayerList()[0];
				var chapeau = joueur.head;
				if (chapeau == hf.Data.HEAD_TUB && this.quetes.estQueteCompletee(2)){
					joueur.head = 13;
					joueur.replayAnim();
					self.keyLock = 68;
				}else if(chapeau == 13){
					joueur.head = joueur.defaultHead;
					joueur.replayAnim();
					self.keyLock = 68;
				}else{
					old(self);
				}
			}else{
				old(self);
			}
		});

	@:diExport
	public var ACTION_BULLE_LORE(default, null): IAction;

	@:diExport
	public var ACTION_CUPIDON(default, null): IAction;

	@:diExport
	public var ACTION_DARKUPIDON(default, null): IAction;

	@:diExport
	public var ACTION_TEMPS_RESET(default, null): IAction;

	@:diExport
	public var PATCH_AMOUR_EXPLOSION(default, null): IPatch;

	@:diExport
	public var EFFET_VIE_SENTIMENTALE(default, null): ISpec;
	@:diExport
	public var EFFET_VIE_LUBRIQUE(default, null): ISpec;
	@:diExport
	public var EFFET_AUREOLE_POUSSIEREUSE(default, null): ISpec;
	@:diExport
	public var EFFET_LETTRE_D_AMOUR(default, null): ISpec;

	public function new(custom_clips: custom_clips.CustomClips, quetes : Quetes){
		this.custom_clips = custom_clips;
		this.quetes = quetes;

		ACTION_BULLE_LORE 	= new BulleLoreAction();
		ACTION_CUPIDON 		= new CupidonAction();
		ACTION_DARKUPIDON 	= new DarkupidonAction();
		ACTION_TEMPS_RESET 	= new TempsReset();

		EFFET_AUREOLE_POUSSIEREUSE = new AureolePoussiereuse();
		EFFET_VIE_LUBRIQUE = new VieLubrique();
		EFFET_VIE_SENTIMENTALE = new VieSentimentale();
		EFFET_LETTRE_D_AMOUR = new LettreDAmour();

		/*PATCH_AMOUR_EXPLOSION = Ref.auto(hf.GameManager.main).wrap(function(hf, self, old) {
			this.custom_clips.substitute("explodeZone", "$explodeCoeur", function() old(self));
		});*/

		PATCH_AMOUR_EXPLOSION = Ref.auto(FxManager.attachFx)
			.wrap(function(hf: Hf, self: FxManager,x: Float, y: Float, link: String, old) {
            if (link == "explodeZone") link = "$explodeCoeur";
            return old(self, x, y, link);
        });
	};
}
