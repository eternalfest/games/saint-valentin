import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;
import better_script.BetterScript;

import atlas.Atlas;

import saint_valentin.SaintValentin;
import quete.Quetes;

import vault.ISpec;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    debug: Debug,
	saintValentin : SaintValentin,
	quete : Quetes,
	betterScript: BetterScript,
	vault: vault.Vault,
    merlin: Merlin,
	atlasDarkness: atlas.props.Darkness,
	atlas: atlas.Atlas,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
