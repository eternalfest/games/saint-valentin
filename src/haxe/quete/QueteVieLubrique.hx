package quete;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import hf.entity.Player;
import etwin.Obfu;
import patchman.HostModuleLoader;

@:build(patchman.Build.di())
class QueteVieLubrique {

	@:diExport
	public var queteVieLubrique(default, null): IPatch;

	public function new(quete: Quetes) {
		this.queteVieLubrique = Ref.auto(hf.entity.Player.initPlayer)
		.before(function(hf, self,g: hf.mode.GameMode, x: Float, y: Float){
			if (quete.estQueteCompletee(1)){
				++self.lives;
			}
		});
	}
}