package quete;

import quete.actions.Quest;

import merlin.IAction;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import hf.entity.Player;
import etwin.Obfu;
import patchman.HostModuleLoader;

@:build(patchman.Build.di())
class Quetes {

	public var hml 				: HostModuleLoader;
	public var data				: patchman.module.Data;
	public var donneesQuetes 	: Array<Dynamic>;
	public var listeQuetes 		: Map<String, Bool>;

	@:diExport
	public var quetes(default, null): IPatch;

	@:diExport
	public var actions(default, null): FrozenArray<IAction>;

	@:diExport
	public var patches(default, null) : IPatch;

	public function new(hml: HostModuleLoader,d : patchman.module.Data) {
		patchman.DebugConsole.log(Obfu.raw('Quetes: new'));
		patches = new PatchList([
      (new QueteVieLubrique(this)).queteVieLubrique,
      (new QueteVieSentimentale(this)).queteVieSentimentale,
      (new QueteAureole(this)).queteAureole
    ]);

		this.data = d;
		var donneesQuetes: Array<Dynamic> = d.get(Obfu.raw("quetes"));
		listeQuetes = new Map();

		this.quetes = Ref
		.auto(hf.mode.GameMode.initWorld)
		.before(function(hf,self): Void {
			patchman.DebugConsole.log(Obfu.raw('Quetes: initWorld'));
			var items = Quetes.getItemsFromHml(hml);
			for (quete in donneesQuetes){
				// ID de la quête
				var id			: String = Reflect.field(quete, Obfu.raw("id"));
				// Objets nécessaire à sa validation
				var require		: Dynamic = Reflect.field(quete, Obfu.raw("require"));
				// Familles débloquées : >= 5000 = clef
				var give		: Null<Array<Int>>= Reflect.field(quete, Obfu.raw("give"));
				// Familles bloquées : >= 5000 = clef
				var remove		: Null<Array<Int>> = Reflect.field(quete, Obfu.raw("remove"));

				if (Quetes.__estQueteCompletee(require, items)){
					listeQuetes.set(id, true);
					patchman.DebugConsole.log('La quête (' + Std.string(id) +') est débloquée.');
					for (famille in give){
						Quetes.debloqueFamille(self, famille);
					}
					for (famille in remove){
						Quetes.bloqueFamille(self, famille);
					}
				}else{
					this.listeQuetes.set(id, false);
					patchman.DebugConsole.log('La quête (' + Std.string(id) +') n\'est pas débloquée.');
				}
			}
		});
		this.actions = FrozenArray.of(cast new Quest(this));
	}

	public static function debloqueFamille(self : hf.mode.GameMode, n : Int) : Void {
		if( n >= 1000 && n < 2000){
			var config = self.root.GameManager.CONFIG.scoreItemFamilies;
				config.push(n);
			self.randMan.register(self.root.Data.RAND_SCORES_ID, self.root.Data.getRandFromFamilies(self.root.Data.SCORE_ITEM_FAMILIES, config));
		}else if (n < 1000) {
			var config = self.root.GameManager.CONFIG.specialItemFamilies;
				config.push(n);
			self.randMan.register(self.root.Data.RAND_ITEMS_ID, self.root.Data.getRandFromFamilies(self.root.Data.SPECIAL_ITEM_FAMILIES, config));
		}else if (n >= 5000){
			self.giveKey(n - 5000);
		}
	}

	public static function bloqueFamille(self : hf.mode.GameMode, n : Int) : Void {
		if( n >= 1000 && n < 2000){
			self.randMan.remove(self.root.Data.RAND_SCORES_ID, n);
		}else if (n < 1000) {
			self.randMan.remove(self.root.Data.RAND_ITEMS_ID, n);
		}else if (n >= 5000){
			self.worldKeys[n - 5000] = false;
		}
	}

	public static function getItemsFromHml(hml: HostModuleLoader): Dynamic {
		var runStartMod: Dynamic = hml.require("run-start");
		var runStartData: Null<Dynamic> = Reflect.callMethod(runStartMod, Reflect.field(runStartMod, Obfu.raw("get")), []);
		if (runStartData == null) {
			patchman.DebugConsole.error("Erreur: la partie n'a pas encore été lancée !");
			return null;
		}
		var items: Dynamic = Reflect.field(runStartData, Obfu.raw("items"));
		return items;
	}

	public static function __estQueteCompletee(necessaire : Dynamic, items : Dynamic) : Bool{
		for (clef in Reflect.fields(necessaire)){
			var n : Int = Reflect.field(necessaire, clef);
			var possession : Int = items[Std.parseInt(clef)];
			if (possession == null)
				possession = 0;
			if (possession < n){
				return false;
			}
		}
		return true;
	}

	public function estQueteCompletee(id : Int) : Bool{
		return listeQuetes.get(Std.string(id));
	}
}
