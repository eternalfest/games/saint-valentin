package quete;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import hf.entity.Player;
import etwin.Obfu;
import patchman.HostModuleLoader;

@:build(patchman.Build.di())
class QueteVieSentimentale {

	@:diExport
	public var queteVieSentimentale(default, null): IPatch;

	public function new(quetes : Quetes) {
		this.queteVieSentimentale = Ref.auto(hf.entity.Player.initPlayer)
		.before(function(hf, self,g: hf.mode.GameMode, x: Float, y: Float){
			if (quetes.estQueteCompletee(0)){
				++self.lives;
			}
		});
	}
}