package quete;

import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import hf.Hf;
import hf.entity.Player;
import etwin.Obfu;
import patchman.HostModuleLoader;
import keyboard.Key;
import keyboard.KeyCode;

@:build(patchman.Build.di())
class QueteAureole {

	@:diExport
	public var queteAureole(default, null): IPatch;

	public function new(quetes : Quetes) {
		this.queteAureole = Ref.auto(hf.mode.GameMode.getControls)
		.before(function(hf, self){
			if (quetes.estQueteCompletee(2) && self.fl_disguise && Key.isDown(KeyCode.D) && self.keyLock != 68){
				var joueur = (self.getPlayerList())[0];
				var save = joueur.head;
				patchman.DebugConsole.log(save);
				joueur.head++;
				if (joueur.head == hf.Data.HEAD_TUB + 1) {
					joueur.head = 13;
				}
				if (joueur.head > 13) {
					joueur.head = joueur.defaultHead;
				}
				if (save != joueur.head) {
					joueur.replayAnim();
				}
				self.keyLock = 68;
			}
		});
	}
}